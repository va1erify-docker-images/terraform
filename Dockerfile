FROM alpine

WORKDIR /workdir

ARG TZ=Europe/Moscow
ARG APK_ADD_PACKAGE="curl unzip bash"
ARG APK_DEL_PACKAGE="unzip"
ARG TERRAFORM_ZIP_URL="https://hashicorp-releases.yandexcloud.net/terraform/1.7.4/terraform_1.7.4_linux_amd64.zip"

COPY terraformrc.config ~/.terraformrc

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime

RUN apk add --no-cache $APK_ADD_PACKAGE
RUN curl -L -o terraform.zip $TERRAFORM_ZIP_URL && \
    unzip terraform.zip  && \
    mv terraform /usr/bin/ && \
    rm terraform.zip
RUN apk del $APK_DEL_PACKAGE

CMD ["/bin/bash"]